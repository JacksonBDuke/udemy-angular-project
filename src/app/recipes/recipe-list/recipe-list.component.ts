import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Spaghetti', 'You better not touch it!', 'https://cdn.pixabay.com/photo/2016/05/14/18/20/spaghetti-1392266_960_720.jpg'),
    new Recipe('Spaghetti', 'You better not touch it!', 'https://cdn.pixabay.com/photo/2016/05/14/18/20/spaghetti-1392266_960_720.jpg')
  ];
  constructor() { }

  ngOnInit() {
  }

}
